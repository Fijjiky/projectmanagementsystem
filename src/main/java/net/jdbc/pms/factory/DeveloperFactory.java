package net.jdbc.pms.factory;

import net.jdbc.pms.dao.DevelopersDAO;

import java.sql.SQLException;

public class DeveloperFactory {
    public void createDeveloper(int developerID, String name, int teamID) throws SQLException {
        DevelopersDAO developerDAO = new DevelopersDAO();
        developerDAO.createElement(developerID, name, teamID);
    }
}
