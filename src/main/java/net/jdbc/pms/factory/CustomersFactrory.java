package net.jdbc.pms.factory;

import net.jdbc.pms.dao.CustomersDAO;

import java.sql.SQLException;

public class CustomersFactrory {
    public void createCustomer(int customerID, String customer_name) throws SQLException {
        CustomersDAO customersDAO = new CustomersDAO();
        customersDAO.createElement(customerID, customer_name);
    }
}
