package net.jdbc.pms.factory;

import net.jdbc.pms.dao.SkillsDAO;

import java.sql.SQLException;

public class SkillsFactory {
    public void createSkill(int sk_id, String sk_name) throws SQLException {
        SkillsDAO skillsDAO = new SkillsDAO();
        skillsDAO.createElement(sk_id, sk_name);
    }
}
