package net.jdbc.pms.factory;

import net.jdbc.pms.dao.CompanyDAO;

import java.sql.SQLException;

public class CompanyFactory {
    public void createCompany(int com_id, String com_name) throws SQLException {
        CompanyDAO companyDAO = new CompanyDAO();
        companyDAO.createElement(com_id, com_name);
    }
}
